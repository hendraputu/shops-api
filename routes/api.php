<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Api\ProductController;
use App\Http\Controllers\Api\BrandController;
use App\Http\Controllers\Api\OrderController;
use App\Http\Controllers\Api\AuthController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::resource('user', UserController::class)->middleware(['auth:api', 'log.route', 'check.role:admin']);
Route::resource('product', ProductController::class)->middleware(['auth:api', 'log.route', 'check.role:admin,warehouse']);
Route::resource('brand', BrandController::class)->middleware(['auth:api', 'log.route', 'check.role:admin,warehouse']);
Route::resource('order', OrderController::class)->middleware('auth:api');

// register
Route::post('/register', [AuthController::class, 'register']);
// login
Route::post('/login', [AuthController::class, 'login']);

Route::get('/product-list', [ProductController::class, 'index']);
Route::get('/product-detail/{id}', [ProductController::class, 'show']);
Route::get('/product-search/{field}/{value}', [ProductController::class, 'search']);
