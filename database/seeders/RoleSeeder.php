<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            [
                'roles' => 'admin',
                'user_id' => 1,
            ],
            [
                'roles' => 'customer',
                'user_id' => 3,
            ],
            [
                'roles' => 'warehouse',
                'user_id' => 2,
            ],
        ]);
    }
}
