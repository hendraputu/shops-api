<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            [
                'brand_id' => 6,
                'name' => 'Xiaomi Mi A2',
                'os' => 'android',
                'photo' => 'https://picsum.photos/id/237/200/300',
                'cpu' => 2,
                'ram' => 4,
                'price' => 300,
                'stock' => 10,
                'status' => 'active'
            ],
            [
                'brand_id' => 3,
                'name' => 'Oppo F1',
                'os' => 'android',
                'photo' => 'https://picsum.photos/id/239/200/300',
                'cpu' => 2,
                'ram' => 3,
                'price' => 200,
                'stock' => 10,
                'status' => 'active'
            ],
            [
                'brand_id' => 1,
                'name' => 'iPhone X',
                'os' => 'ios',
                'photo' => 'https://picsum.photos/id/240/200/300',
                'cpu' => 2,
                'ram' => 4,
                'price' => 700,
                'stock' => 10,
                'status' => 'active'
            ],
            [
                'brand_id' => 1,
                'name' => 'iPhone 6',
                'os' => 'ios',
                'photo' => 'https://picsum.photos/id/241/200/300',
                'cpu' => 2,
                'ram' => 3,
                'price' => 300,
                'stock' => 10,
                'status' => 'active'
            ],
            [
                'brand_id' => 4,
                'name' => 'Samsung Galaxy X',
                'os' => 'android',
                'photo' => 'https://picsum.photos/id/242/200/300',
                'cpu' => 2,
                'ram' => 4,
                'price' => 500,
                'stock' => 10,
                'status' => 'active'
            ],
        ]);
    }
}
