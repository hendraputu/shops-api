<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Brand;

class BrandSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('brands')->insert([
            ['name' => 'Apple'],
            ['name' => 'Google Pixel'],
            ['name' => 'Oppo'],
            ['name' => 'Samsung'],
            ['name' => 'Sony'],
            ['name' => 'Xiaomi'],
        ]);
        // Brand::factory()->times(10)->create();
    }
}
