<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('orders')->insert([
            [
                'user_id' => 3,
                'total_price' => 500,
                'invoice_number' => 'INV0001',
                'status' => 'shipped',
            ],
            [
                'user_id' => 2,
                'total_price' => 400,
                'invoice_number' => 'INV0002',
                'status' => 'cancelled',
            ],
        ]);
    }
}
