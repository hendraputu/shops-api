<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Brand;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $photo = $this->faker->numberBetween(100, 500);
        $picsum = "https://picsum.photos/id/" . $photo . "/200/300";
        return [
            'brand_id' => Brand::factory(),
            'name' => $this->faker->name,
            'os' => $this->faker->randomElement(['android', 'ios']),
            'photo' => $picsum,
            'cpu' => $this->faker->randomDigitNot(0),
            'ram' => $this->faker->randomDigitNot(0),
            'price' => $this->faker->randomFloat(2, 100, 1000),
            'stock' => $this->faker->randomDigit,
            'status' => 'active'
        ];
    }
}
