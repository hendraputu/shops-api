<?php

namespace Database\Factories;

use App\Models\Order;
use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\User;

class OrderFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Order::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $inv = $this->faker->randomNumber();
        return [
            'user_id' => User::factory(),
            'total_price' => 0,
            'invoice_number' => "INV0$inv",
            'status' => 'new',
        ];
    }
}
