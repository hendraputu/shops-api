<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        return response()->json($products);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate data
        $validateData = $request->validate([
            'brand_id' => 'required|integer|min:1|exists:brands,id',
            'name' => 'required|string|max:255',
            'os' => 'required|string|in:android,ios',
            'photo' => 'required|string|max:255|active_url',
            'cpu' => 'required|integer|min:1',
            'ram' => 'required|integer|min:1',
            'price' => 'required|numeric|min:0.01|max:9999999.99',
            'stock' => 'required|integer|min:1',
            'status' => 'required|string|in:active,inactive',
        ]);
        $product = Product::create($validateData);
        return response()->json($product);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::find($id);
        return response()->json($product);
    }

    public function search($field, $value)
    {
        $product = Product::where($field, 'like', "%$value%")->get();

        return response()->json($product);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validateData = $request->validate([
            'brand_id' => 'integer|min:1|exists:brands,id',
            'name' => 'string|max:255',
            'os' => 'string|in:android,ios',
            'photo' => 'string|max:255|active_url',
            'cpu' => 'integer|min:1',
            'ram' => 'integer|min:1',
            'price' => 'numeric|min:0.01|max:9999999.99',
            'stock' => 'integer|min:1',
            'status' => 'string|in:active,inactive',
        ]);
        $product = Product::find($id);
        $product->update($validateData);

        return response()->json($product);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        $product->delete();
        return response(['message' => 'Product is success deleted']);
    }
}
