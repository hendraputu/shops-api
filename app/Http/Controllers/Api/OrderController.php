<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Order;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::all();
        $orderList = [];
        $i = 0;
        foreach ($orders as $order) {
            $order['products'] = $order->products;
            $orderList[$i] = $order;
            $i++;
        }

        return response()->json($orderList);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateOrder = $request->validate([
            'user_id' => 'required|integer|min:1|exists:users,id',
            'invoice_number' => 'required|string|max:255',
            'total_price' => 'required|numeric|min:0.01|max:9999999.99',
            'status' => 'required|string|in:new,shipped,cancelled',
            'products.*.product_id' => 'required|integer|min:1|exists:products,id',
            'products.*.quantity' => 'required|integer|min:1'
        ]);

        $order = Order::create($validateOrder);
        $orderProduct = [];
        $i = 0;
        foreach ($validateOrder['products'] as $product) {
            $orderProduct[$i] = [
                'order_id' => $order->id,
                'product_id' => $product['product_id'],
                'quantity' => $product['quantity']
            ];
            $i++;
        }
        $order->order_products()->createMany($orderProduct);
        $order->refresh();

        return response()->json($order);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::find($id);
        $order['products'] = $order->products;
        return response()->json($order);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validateOrder = $request->validate([
            'products.*.product_id' => 'integer|min:1|exists:products,id',
            'products.*.quantity' => 'integer|min:1'
        ]);

        'App\Models\OrderProduct'::where('order_id', $id)->delete();

        $order = Order::find($id);
        $orderProduct = [];
        $i = 0;
        foreach ($validateOrder['products'] as $product) {
            $orderProduct[$i] = [
                'order_id' => $order->id,
                'product_id' => $product['product_id'],
                'quantity' => $product['quantity']
            ];
            $i++;
        }
        $order->order_products()->createMany($orderProduct);
        $order->refresh();

        return response()->json($order);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $order = Order::find($id);
        $order->delete();
        return response(['message' => 'Order deleted successfully']);
    }
}
