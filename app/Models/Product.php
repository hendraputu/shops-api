<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        'brand_id',
        'name',
        'os',
        'photo',
        'cpu',
        'ram',
        'price',
        'stock',
        'status'
    ];

    public function orders()
    {
        return $this->belongsToMany('App\Models\Order');
    }

    public function brands()
    {
        return $this->belongsTo('App\Models\Brand');
    }
}
