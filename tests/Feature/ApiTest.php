<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;

class ApiTest extends TestCase
{
    // use RefreshDatabase;

    protected $customerUser;
    protected $warehouseUser;
    protected $adminUser;
    protected $adminToken;
    protected $customerToken;
    protected $warehouseToken;

    public function setUp(): void
    {
        parent::setUp();
        // Artisan::call('passport:install');
        $customer = User::factory()->create();
        $warehouse = User::factory()->create(['is_admin' => 0, 'is_warehouse' => 1]);
        $admin = User::factory()->create(['is_admin' => 1, 'is_warehouse' => 0]);
        $this->customerUser = $customer;
        $this->warehouseUser = $warehouse;
        $this->adminUser = $admin;
        $this->adminToken = $this->adminUser->createToken('authToken')->accessToken;
        $this->customerUser = $this->adminUser->createToken('authToken')->accessToken;
        $this->warehouseUser = $this->adminUser->createToken('authToken')->accessToken;
        DB::table('roles')->insert([
            [
                'roles' => 'admin',
                'user_id' => $admin->id,
            ],
            [
                'roles' => 'customer',
                'user_id' => $customer->id,
            ],
            [
                'roles' => 'warehouse',
                'user_id' => $warehouse->id,
            ],
        ]);
    }

    protected function withAdminToken()
    {
        return $this->withHeaders(['Authorization' => "Bearer $this->adminToken"]);
    }

    protected function withCustomerToken()
    {
        return $this->withHeaders(['Authorization' => "Bearer $this->customerToken"]);
    }

    protected function withWarehouseToken()
    {
        return $this->withHeaders(['Authorization' => "Bearer $this->warehouseToken"]);
    }

    protected function actingAsUser(User $user)
    {
        $accessToken = $user->createToken('authToken')->accessToken;
        return $this->withHeaders(['Authorization' => "Bearer $accessToken"]);
    }

    public function testBasicTest()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    // public function testUserCanLogin()
    // {
    //     $loginData = [
    //         'email' => $this->customerUser->email,
    //         'password' => $this->customerUser->password
    //     ];
    //     $response = $this->json('POST', '/api/login', $loginData);
    //     $response->assertStatus(200);
    // }

    // price: ranges between 100 to 1000
    // user buyers:
    // testBuyersCanViewAllProduct
    // testSearchByProductName
    // testSearchByProductOS
    // testSearchByProductRAM
    // testSearchByProductPrice (min and max)
    // testRegisterBuyer
    // testLogin
    // testCanAddMax2Products
    // testCanRemoveProduct
    // testCreateOrder
    // testCanNotLessThan100
    // testSendEmailToWarehouse
    // testSendThankToCustomer
    // testClearShoppingCart
    // testGetAllOrderByUser
}
