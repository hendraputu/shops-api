<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Order;
use App\Models\User;
use App\Models\Product;
use Illuminate\Support\Facades\DB;

class OrderTest extends ApiTest
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    protected $url = '/api/order';

    public function testCustomerCanViewOrder()
    {
        $user = User::factory()->create();
        $product = Product::factory()->create();
        $order = Order::factory()->create(['user_id' => $user->id]);
        DB::table('order_product')->insert([
            'order_id' => $order->id,
            'product_id' => $product->id,
            'quantity' => 1
        ]);
        $token = $user->createToken('authToken')->accessToken;
        $response = $this->withHeaders(['Authorization' => "Bearer $token"])->json('GET', $this->url);
        $response->assertStatus(200);
    }
}
