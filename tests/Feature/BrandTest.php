<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Brand;

class BrandTest extends ApiTest
{
    /**
     * A basic feature test example.
     *
     * @return void
     */

    protected $url = '/api/brand';

    public function testCanViewAllBrand()
    {
        Brand::factory()->count(5)->create();
        $response = $this->withAdminToken()->json('GET', $this->url);
        $response->assertStatus(200);
    }

    public function testCanViewBrand()
    {
        $brand = Brand::factory()->create();
        $response = $this->withAdminToken()->json('GET', "$this->url/$brand->id");
        $response->assertStatus(200);
    }

    public function testCanAddBrand()
    {
        $brand = Brand::factory()->make()->toarray();
        $response = $this->withAdminToken()->json('post', $this->url, $brand);
        $response->assertStatus(200);
    }

    public function testCanEditBrand()
    {
        $brand = Brand::factory()->create();
        $brandEdit = Brand::factory()->make()->toarray();
        $response = $this->withAdminToken()->json('put', "$this->url/$brand->id", $brandEdit);
        $response->assertStatus(200);
    }

    public function testCanDeleteBrand()
    {
        $brand = Brand::factory()->create();
        $response = $this->withAdminToken()->json('delete', "$this->url/$brand->id");
        $response->assertStatus(200);
    }
}
