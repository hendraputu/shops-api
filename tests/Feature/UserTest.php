<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;

class UserTest extends ApiTest
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    protected $url = '/api/user';

    public function testAdminCanSeeAllUser()
    {
        $user = User::factory()->count(10)->create();
        $response = $this->withAdminToken()->json('get', $this->url);
        $response->assertStatus(200);
    }

    public function testAdminCanSeeUserDetail()
    {
        $user = User::factory()->create();
        $response = $this->withAdminToken()->json('get', "$this->url/$user->id");
        $response->assertStatus(200);
    }

    public function testAdminCanAddUser()
    {
        $user = User::factory()->make()->toarray();
        $user['password'] = 'secret';
        $user['password_confirmation'] = 'secret';
        $response = $this->withAdminToken()->json('post', $this->url, $user);
        $response->assertStatus(200);
    }

    public function testAdminCanEditUser()
    {
        $user = User::factory()->create();
        $userEdit = [
            'password' => 'password_test',
            'password_confirmation' => 'password_test',
        ];
        $response = $this->withAdminToken()->json('put', "$this->url/$user->id", $userEdit);
        $response->assertStatus(200);
    }

    public function testAdminCanDeleteUser()
    {
        $user = User::factory()->create();
        $response = $this->withAdminToken()->json('delete', "$this->url/$user->id");
        $response->assertStatus(200);
    }
}
