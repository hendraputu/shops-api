<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Product;

class ProductTest extends ApiTest
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    protected $url = '/api/product';

    public function testGuestCanSeeAllProduct()
    {
        Product::factory()->count(10)->create();
        $response = $this->get("$this->url-list");
        $response->assertStatus(200);
    }

    public function testGuestCanSeeProductDetail()
    {
        $product = Product::factory()->create();
        $response = $this->get("$this->url-detail/$product->id");
        $response->assertStatus(200);
    }

    public function testAdminCanSeeAllProduct()
    {
        Product::factory()->count(10)->create();
        $response = $this->withAdminToken()->json('get', $this->url);
        $response->assertStatus(200);
    }

    public function testAdminCanSeeProductDetail()
    {
        $product = Product::factory()->create();
        $response = $this->withAdminToken()->json('get', "$this->url/$product->id");
        $response->assertStatus(200);
    }

    public function testAdminCanAddProduct()
    {
        $product = Product::factory()->make()->toarray();
        $response = $this->withAdminToken()->json('post', $this->url, $product);
        $response->assertStatus(200);
    }

    public function testAdminCanEditProduct()
    {
        $product = Product::factory()->create();
        $productEdit = Product::factory()->make()->toarray();
        $response = $this->withAdminToken()->json('put', "$this->url/$product->id", $productEdit);
        $response->assertStatus(200);
    }

    public function testAdminCanDeleteProduct()
    {
        $product = Product::factory()->create();
        $response = $this->withAdminToken()->json('delete', "$this->url/$product->id");
        $response->assertStatus(200);
    }

    public function testSearchProduct()
    {
        Product::factory()->create(['name' => 'samsung galak']);
        Product::factory()->create(['name' => 'apel mecegut']);
        Product::factory()->create(['name' => 'xi oh mie']);
        $endpoint = '/api/product-search/os/android';
        $response = $this->json('get', $endpoint);
        $response->assertStatus(200);
        $endpoint = '/api/product-search/name/apel';
        $response = $this->json('get', $endpoint);
        $response->assertStatus(200);
    }
}
